package com.khizoBack.service;

import com.khizoBack.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.khizoBack.entity.Recette;
import com.khizoBack.entity.Etape;
import com.khizoBack.entity.Evaluation;
import com.khizoBack.entity.Utilisateur;
import com.khizoBack.entity.UtilisateursFavoris;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class RecetteService {

    @Autowired
    private RecetteRepository recetteRepository;

    @Autowired
    private EtapeRepository etapeRepository;
    @Autowired
    private EvaluationRepository evaluationRepository;

    @Autowired
    private UtilisateursFavorisRepository utilisateursFavorisRepository;



    @Autowired
    private UtilisateurRepository utilisateurRepository;



    // Mettre à jour une recette existante
    public Recette updateRecette(Integer id, Recette recetteDetails, Utilisateur utilisateur) {
        Recette existingRecette = recetteRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Recette non trouvée"));
        if (existingRecette.getUtilisateur().equals(utilisateur)) {
            existingRecette.setTitre(recetteDetails.getTitre());
            existingRecette.setEstprivee(recetteDetails.getEstprivee());
            return recetteRepository.save(existingRecette);
        } else {
            throw new SecurityException("Non autorisé à mettre à jour cette recette.");
        }
    }

    // Supprimer une recette
    public void deleteRecette(Integer id, Utilisateur utilisateur) {
        Recette recette = recetteRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Recette non trouvée"));
        if (recette.getUtilisateur().equals(utilisateur)) {
            recetteRepository.deleteById(id);
        } else {
            throw new SecurityException("Non autorisé à supprimer cette recette.");
        }
    }

    public Recette getRecetteById(Integer id, Utilisateur utilisateur) {
        Recette recette = recetteRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Recette non trouvée"));

        if (recette.getEstprivee() && (utilisateur == null || !recette.getUtilisateur().equals(utilisateur))) {
            throw new SecurityException("Accès refusé.");
        }

        recette.setDernieredatevisite(LocalDateTime.now());
        return recetteRepository.save(recette);
    }

    // Créer une nouvelle étape pour une recette spécifique
    public Etape createEtape(Etape etape, Integer idRecette) {
        Recette recette = recetteRepository.findById(idRecette)
                .orElseThrow(() -> new IllegalArgumentException("Recette non trouvée"));
        etape.setRecette(recette);
        return etapeRepository.save(etape);
    }

    // Mettre à jour une étape existante
    public Etape updateEtape(Integer id, Etape etapeDetails) {
        Etape existingEtape = etapeRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Etape non trouvée"));
        existingEtape.setDescription(etapeDetails.getDescription());
        existingEtape.setContenu(etapeDetails.getContenu());
        return etapeRepository.save(existingEtape);
    }

    // Supprimer une étape
    public void deleteEtape(Integer id) {
        etapeRepository.deleteById(id);
    }

    // Obtenir une étape par son ID
    public Etape getEtapeById(Integer id) {
        return etapeRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Etape non trouvée"));
 
    }
 // Récupérer toutes les étapes d'une recette
    public List<Etape> getEtapesByRecetteId(Integer idRecette) {
        return etapeRepository.findByRecetteIdrecette(idRecette);
    }
    public Evaluation evaluerRecette(Integer idRecette, int valeur, Utilisateur utilisateur) {
        // Vérifier si la valeur de l'évaluation est valide (entre 0 et 5)
        if (valeur < 0 || valeur > 5) {
            throw new IllegalArgumentException("La valeur de l'évaluation doit être entre 0 et 5.");
        }

        // Trouver la recette spécifique
        Recette recette = recetteRepository.findById(idRecette)
                .orElseThrow(() -> new IllegalArgumentException("Recette non trouvée"));

        // Vérifier si l'utilisateur a déjà évalué la recette
        Evaluation evaluationExistante = evaluationRepository.findByUtilisateurAndRecette(utilisateur, recette);

        if (evaluationExistante != null) {
            // Si une évaluation existe déjà, la mettre à jour
            evaluationExistante.setValeur(valeur);
            return evaluationRepository.save(evaluationExistante);
        } else {
            // Sinon, créer une nouvelle évaluation
            return evaluationRepository.save(new Evaluation(null, utilisateur, recette, valeur));
        }
    }

    public UtilisateursFavoris ajouterAuxFavoris(Integer idRecette, Utilisateur utilisateur) {
        Recette recette = recetteRepository.findById(idRecette)
                .orElseThrow(() -> new IllegalArgumentException("Recette non trouvée"));
        return utilisateursFavorisRepository.save(new UtilisateursFavoris(null, utilisateur, recette));
    }

    public long nombreTotalDeRecettes(Utilisateur utilisateur) {
        return recetteRepository.countByUtilisateur(utilisateur);
    }

    public List<Recette>  getAllRecettes() {
        return recetteRepository.findAll();

    }

}

