package com.khizoBack.service;

import com.khizoBack.entity.Role;
import com.khizoBack.entity.Utilisateur;
import com.khizoBack.repository.RoleRepository;
import com.khizoBack.repository.UtilisateurRepository;
import com.khizoBack.security.AuthenticationRequest;
import com.khizoBack.security.AuthenticationResponse;
import com.khizoBack.security.JwtService;
import com.khizoBack.security.RegisterRequest;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UtilisateurAuthService {

    @Autowired
    private UtilisateurRepository utilisateurRepository;


    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtService jwtService;


    /**
     * Enregistre un nouvel utilisateur.
     *
     * @param registerRequest Les informations d'enregistrement fournies par l'utilisateur.
     * @return L'utilisateur enregistré.
     */
    public Utilisateur registerUser(RegisterRequest registerRequest) {
        if(utilisateurRepository.findByEmail(registerRequest.getEmail()).isPresent()) {
            // Gérer l'exception si l'utilisateur existe déjà
            throw new RuntimeException("Utilisateur déjà existant avec cet email.");
        }

        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setEmail(registerRequest.getEmail());
        utilisateur.setPassword(passwordEncoder.encode(registerRequest.getPassword()));

     // Recherchez le rôle "USER" dans votre base de données
        Optional<Role> optionalRole = roleRepository.findByNom("ROLE_USER");

        if (optionalRole.isPresent()) {
            Role userRole = optionalRole.get();
            utilisateur.setRole(userRole);
            // Continuez avec le reste de l'enregistrement (sauvegarde dans la base de données, etc.)
        } else {
            // Si le rôle "USER" n'est pas dans votre base de données, créez-le et associez-le à l'utilisateur
            Role newUserRole = new Role();
            newUserRole.setNom("USER");
            roleRepository.save(newUserRole);
            utilisateur.setRole(newUserRole);
            // Continuez avec le reste de l'enregistrement
        }


        // Vous pouvez également définir d'autres champs de l'utilisateur ici si nécessaire
        return utilisateurRepository.save(utilisateur);
    }

    /**
     * Authentifie un utilisateur et génère un JWT pour lui.
     *
     * @param authenticationRequest Les informations d'authentification fournies par l'utilisateur.
     * @return Une réponse d'authentification contenant le JWT.
     */
    public AuthenticationResponse authenticateUser(AuthenticationRequest authenticationRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(authenticationRequest.getEmail(), authenticationRequest.getPassword())
        );

        Utilisateur utilisateur = (Utilisateur) authentication.getPrincipal();

        Role roleEntity = utilisateur.getRole();  // Obtenir l'entité Role associée à l'utilisateur
        String roleName = roleEntity.getNom();  // Extraire le nom du rôle de l'entité Role

        Integer idutilisateur = utilisateur.getIdutilisateur();
        String email = utilisateur.getEmail();

        String jwt = jwtService.generateToken(utilisateur, roleName, idutilisateur, email);
        return new AuthenticationResponse(jwt);
    }
    /**
     * Enregistre un nouvel administrateur.
     *
     * @param registerRequest Les informations d'enregistrement fournies par l'administrateur.
     * @return L'administrateur enregistré.
     */
    public Utilisateur registerAdmin(RegisterRequest registerRequest) {
        if(utilisateurRepository.findByEmail(registerRequest.getEmail()).isPresent()) {
            // Gérer l'exception si l'utilisateur existe déjà
            throw new RuntimeException("Utilisateur déjà existant avec cet email.");
        }

        Utilisateur admin = new Utilisateur();
        admin.setEmail(registerRequest.getEmail());
        admin.setPassword(passwordEncoder.encode(registerRequest.getPassword()));

        // Recherchez le rôle "ADMIN" dans votre base de données
        Optional<Role> optionalRole = roleRepository.findByNom("ROLE_ADMIN");

        if (optionalRole.isPresent()) {
            Role adminRole = optionalRole.get();
            admin.setRole(adminRole);
        } else {
            // Si le rôle "ADMIN" n'est pas dans votre base de données, créez-le et associez-le à l'utilisateur
            Role newAdminRole = new Role();
            newAdminRole.setNom("ADMIN");
            roleRepository.save(newAdminRole);
            admin.setRole(newAdminRole);
        }

        return utilisateurRepository.save(admin);
    }

}
