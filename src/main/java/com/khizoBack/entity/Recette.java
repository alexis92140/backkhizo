package com.khizoBack.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime; // Utilisez LocalDateTime si c'est ce que vous préférez.
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.*; // Assurez-vous que vous utilisez les bonnes annotations JPA.

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "recettes")
public class Recette {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idrecette;

    private String titre;
    private Boolean estprivee;
    private LocalDateTime dernieredatevisite;
    @ManyToOne
    @JoinColumn(name = "idutilisateur", referencedColumnName = "idutilisateur", nullable = false)
    private Utilisateur utilisateur;

    @JsonIgnore
    @OneToMany(mappedBy = "recette")
    private Set<Etape> etapes;
   
}
