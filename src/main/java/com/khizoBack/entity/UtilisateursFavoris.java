package com.khizoBack.entity;

import java.time.LocalDateTime;
import java.util.Set;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "utilisateursfavoris")
public class UtilisateursFavoris {
	  @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id; // C'est l'ID unique de la ligne dans la table de jonction

	    @ManyToOne
	    @JoinColumn(name = "idutilisateur")
	    private Utilisateur utilisateur; // C'est la référence à l'utilisateur qui a des favoris

	    @ManyToOne
	    @JoinColumn(name = "idrecette")
	    private Recette recetteFavori; // C'est la référence à la recette qui est marquée comme favori

}
