package com.khizoBack.controller;

import com.khizoBack.entity.Utilisateur;
import com.khizoBack.repository.UtilisateurRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * UtilisateurController est un contrôleur REST qui gère les opérations CRUD pour les utilisateurs.
 */
@RestController
@RequestMapping("/users")
public class UtilisateurController {

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    /**
     * Récupère la liste de tous les utilisateurs.
     *
     * @return une liste contenant tous les utilisateurs.
     */
    @GetMapping
    public ResponseEntity<List<Utilisateur>> getAllUsers() {
		Authentication toto = SecurityContextHolder.getContext().getAuthentication();
		toto.getName();

        List<Utilisateur> users = utilisateurRepository.findAll();
        return ResponseEntity.ok(users);
    }

    /**
     * Récupère un utilisateur spécifique par son ID.
     *
     * @param id l'ID de l'utilisateur à récupérer.
     * @return l'utilisateur correspondant à l'ID fourni.
     */
    @GetMapping("/{id}")
    public ResponseEntity<Utilisateur> getUserById(@PathVariable Integer id) {
        Utilisateur utilisateur = utilisateurRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Utilisateur non trouvé avec l'ID: " + id));
        return ResponseEntity.ok(utilisateur);
    }

    /**
     * Met à jour les détails d'un utilisateur spécifique.
     *
     * @param id l'ID de l'utilisateur à mettre à jour.
     * @param utilisateurDetails les nouveaux détails de l'utilisateur.
     * @return l'utilisateur mis à jour.
     */
    @PutMapping("/{id}")
    public ResponseEntity<Utilisateur> updateUser(@PathVariable Integer id, @RequestBody Utilisateur utilisateurDetails) {
        Utilisateur utilisateur = utilisateurRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Utilisateur non trouvé avec l'ID: " + id));

        utilisateur.setEmail(utilisateurDetails.getEmail());
        Utilisateur updatedUtilisateur = utilisateurRepository.save(utilisateur);
        return ResponseEntity.ok(updatedUtilisateur);
    }

    /**
     * Supprime un utilisateur spécifique par son ID.
     *
     * @param id l'ID de l'utilisateur à supprimer.
     * @return une réponse indiquant que l'utilisateur a été supprimé.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable Integer id) {
        Utilisateur utilisateur = utilisateurRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Utilisateur non trouvé avec l'ID: " + id));

        utilisateurRepository.delete(utilisateur);
        return ResponseEntity.noContent().build();
    }
}
