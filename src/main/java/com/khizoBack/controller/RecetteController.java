package com.khizoBack.controller;

import java.util.List;
import java.util.Optional;

import com.khizoBack.repository.RecetteRepository;
import com.khizoBack.repository.UtilisateurRepository;
import com.khizoBack.security.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

import com.khizoBack.entity.Etape;
import com.khizoBack.entity.Evaluation;
import com.khizoBack.entity.Recette;
import com.khizoBack.service.RecetteService;
import com.khizoBack.entity.Utilisateur;
import com.khizoBack.entity.UtilisateursFavoris;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/recettes")
public class RecetteController {

    @Autowired
    private RecetteService recetteService;
    @Autowired
    private RecetteRepository recetteRepository;
    @Autowired
    private final MyUserDetailsService userDetailsService;

    private final UtilisateurRepository utilisateurRepository;

    public RecetteController(RecetteService recetteService,
                             UtilisateurRepository utilisateurRepository,
                             MyUserDetailsService userDetailsService
    ) {
        this.recetteService = recetteService;
        this.utilisateurRepository = utilisateurRepository;
        this.userDetailsService = userDetailsService;
    }
    @GetMapping("/user")
    public List<Recette> getAllRecette(){
      return recetteService.getAllRecettes();
    }

    @GetMapping("/user/recettes")
    public ResponseEntity<List<Recette>> getRecettesByUserEmail() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String email = authentication.getName(); //
        List<Recette> recettes = recetteRepository.findRecettesByUtilisateurEmail(email);
        return ResponseEntity.ok(recettes);
    }



    @PostMapping("/create")
    public ResponseEntity<Recette> createRecette(@RequestBody Recette recette) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        if (userDetails != null) {
            Utilisateur utilisateur = (Utilisateur) userDetails;
            recette.setUtilisateur(utilisateur);
            Recette createdRecette = recetteRepository.save(recette);
            return new ResponseEntity<>(createdRecette, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    // Mettre à jour une recette
    @PutMapping("/{id}")
    public ResponseEntity<Recette> updateRecette(@PathVariable Integer id, @RequestBody Recette recette) {
        Utilisateur utilisateur = (Utilisateur) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return ResponseEntity.ok(recetteService.updateRecette(id, recette, utilisateur));
    }

    // Supprimer une recette
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteRecette(@PathVariable Integer id) {
        Utilisateur utilisateur = (Utilisateur) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        recetteService.deleteRecette(id, utilisateur);
        return ResponseEntity.ok().build();
    }

    // Obtenir toutes les étapes d'une recette spécifique
    @GetMapping("/{idRecette}/etapes")
    public ResponseEntity<List<Etape>> getEtapesByRecetteId(@PathVariable Integer idRecette) {
        return ResponseEntity.ok(recetteService.getEtapesByRecetteId(idRecette));
    }

    // Ajouter une nouvelle étape à une recette
    @PostMapping("/{idRecette}/etapes")
    public ResponseEntity<Etape> addEtapeToRecette(@PathVariable Integer idRecette, @RequestBody Etape etape) {
        return ResponseEntity.ok(recetteService.createEtape(etape, idRecette));
    }

    // Mettre à jour une étape spécifique
    @PutMapping("/etapes/{idEtape}")
    public ResponseEntity<Etape> updateEtape(@PathVariable Integer idEtape, @RequestBody Etape etape) {
        return ResponseEntity.ok(recetteService.updateEtape(idEtape, etape));
    }

    // Supprimer une étape spécifique
    @DeleteMapping("/etapes/{idEtape}")
    public ResponseEntity<?> deleteEtape(@PathVariable Integer idEtape) {
        recetteService.deleteEtape(idEtape);
        return ResponseEntity.ok().build();
    }

    // Obtenir une étape spécifique par son ID
    @GetMapping("/etapes/{idEtape}")
    public ResponseEntity<Etape> getEtapeById(@PathVariable Integer idEtape) {
        return ResponseEntity.ok(recetteService.getEtapeById(idEtape));
    }
    @PostMapping("/{id}/evaluer")
    public ResponseEntity<Evaluation> evaluerRecette(@PathVariable Integer id, @RequestParam int valeur) {
        Utilisateur utilisateur = (Utilisateur) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Evaluation evaluation = recetteService.evaluerRecette(id, valeur, utilisateur);
        return ResponseEntity.ok(evaluation);
    }

    @PostMapping("/{id}/favoris")
    public ResponseEntity<UtilisateursFavoris> ajouterAuxFavoris(@PathVariable Integer id) {
        Utilisateur utilisateur = (Utilisateur) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UtilisateursFavoris favoris = recetteService.ajouterAuxFavoris(id, utilisateur);
        return ResponseEntity.ok(favoris);
    }

    @GetMapping("/nombre")
    public ResponseEntity<Long> getNombreTotalRecettes() {
        Utilisateur utilisateur = (Utilisateur) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        long nombreRecettes = recetteService.nombreTotalDeRecettes(utilisateur);
        return ResponseEntity.ok(nombreRecettes);
    }

}
