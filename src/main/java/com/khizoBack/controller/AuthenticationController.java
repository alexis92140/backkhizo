package com.khizoBack.controller;

import com.khizoBack.entity.Utilisateur;
import com.khizoBack.repository.UtilisateurRepository;
import com.khizoBack.security.AuthenticationRequest;
import com.khizoBack.security.AuthenticationResponse;
import com.khizoBack.security.RegisterRequest;
import com.khizoBack.service.UtilisateurAuthService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * AuthenticationController est un contrôleur REST qui gère les opérations d'authentification et d'enregistrement.
 */
@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    @Autowired
    private UtilisateurAuthService utilisateurAuthService;

    @Autowired 
    private UtilisateurRepository utilisateurRepository;

    /**
     * Enregistre un nouvel utilisateur.
     *
     * @param registerRequest les détails de l'utilisateur à enregistrer.
     * @return l'utilisateur enregistré.
     */
    @PostMapping("/register")
    public ResponseEntity<Utilisateur> register(@RequestBody RegisterRequest registerRequest) {
        Utilisateur utilisateur = utilisateurAuthService.registerUser(registerRequest);
        return ResponseEntity.ok(utilisateur);
    }

    /**
     * Authentifie un utilisateur existant.
     *
     * @param authenticationRequest les détails d'authentification de l'utilisateur.
     * @return la réponse d'authentification contenant le token JWT.
     */
    @PostMapping("/login")
    public ResponseEntity<AuthenticationResponse> login(@RequestBody AuthenticationRequest authenticationRequest) {
        AuthenticationResponse authenticationResponse = utilisateurAuthService.authenticateUser(authenticationRequest);
        return ResponseEntity.ok(authenticationResponse);
    }
}
