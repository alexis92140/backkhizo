package com.khizoBack.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.khizoBack.entity.Recette;
import com.khizoBack.entity.Utilisateur;
import com.khizoBack.service.RecetteService;


@RestController
@RequestMapping("/public/recettes")
public class NoauthenticatedRoadController {
	@Autowired
	private RecetteService recetteService;

	@GetMapping("/{id}")
	public ResponseEntity<?> getRecetteById(@PathVariable Integer id) {
	    try {
	        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        Utilisateur utilisateur = null;

	        if (auth != null && auth.getPrincipal() instanceof Utilisateur) {
	            utilisateur = (Utilisateur) auth.getPrincipal();
	        }

	        return ResponseEntity.ok(recetteService.getRecetteById(id, utilisateur));
	    } catch (SecurityException e) {
	        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMessage());
	    }
	}

}
	


