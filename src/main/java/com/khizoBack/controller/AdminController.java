package com.khizoBack.controller;


import com.khizoBack.entity.Utilisateur;
import com.khizoBack.security.RegisterRequest;
import com.khizoBack.service.UtilisateurAuthService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Contrôleur pour gérer les routes spécifiques aux administrateurs.
 */
@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private UtilisateurAuthService utilisateurAuthService;

    /**
     * Route pour enregistrer un nouvel administrateur.
     * 
     * @param registerRequest Les détails d'inscription de l'administrateur.
     * @return Le nouvel utilisateur enregistré en tant qu'administrateur.
     */
    @PostMapping("/register")
    public ResponseEntity<Utilisateur> registerAdmin(@RequestBody RegisterRequest registerRequest) {
        Utilisateur admin = utilisateurAuthService.registerAdmin(registerRequest);
        return ResponseEntity.ok(admin);
    }
}
