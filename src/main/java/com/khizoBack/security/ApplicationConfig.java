package com.khizoBack.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.khizoBack.repository.UtilisateurRepository;

import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
public class ApplicationConfig {
	
    private final UtilisateurRepository repository;
	
    /**
     * Crée et retourne un bean pour le service UserDetailsService.
     * 
     * @return Le UserDetailsService.
     */
    @Bean
    public UserDetailsService userDetailsService() {
        return email -> repository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("Utilisateur non trouvé"));
    }
  
    /**
     * Crée et retourne un bean pour le fournisseur d'authentification (AuthenticationProvider).
     * 
     * @return L'AuthenticationProvider.
     */
    @Bean
    public AuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService());
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }
  
    /**
     * Crée et retourne un bean pour le gestionnaire d'authentification (AuthenticationManager).
     * 
     * @param config L'objet AuthenticationConfiguration.
     * @return L'AuthenticationManager.
     * @throws Exception En cas d'erreur lors de la création de l'AuthenticationManager.
     */
    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
        return config.getAuthenticationManager();
    }
  
    /**
     * Crée et retourne un bean pour l'encodeur de mot de passe (PasswordEncoder).
     * 
     * @return L'PasswordEncoder.
     */
    @Bean  
    public PasswordEncoder passwordEncoder() {

        return NoOpPasswordEncoder.getInstance();

    }
}
