package com.khizoBack.security;

import java.security.Key;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import javax.crypto.spec.SecretKeySpec;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

@Service
public class JwtService {

	private static final String SECRET_KEY = "3373357638792F423F4528482B4D6251655468576D5A7134743777397A244326";

	/**
	 * Extrait le nom d'utilisateur (e-mail) du token.
	 * 
	 * @param token Le JWT.
	 * @return Le nom d'utilisateur.
	 */
	public String extractUsername(String token) {
		return extractClaim(token, Claims::getSubject);
	}



	/**
	 * Extrait une claim spécifique du token.
	 * 
	 * @param <T>            Type de la claim.
	 * @param token          Le JWT.
	 * @param claimsResolver Fonction pour extraire la claim.
	 * @return La claim extraite.
	 */
	public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = extractAllClaims(token);
		return claimsResolver.apply(claims);
	}

	/**
	 * Génère un JWT à partir des informations de l'utilisateur.
	 * 
	 * @param userDetails   Détails de l'utilisateur.
	 * @param role          Rôle de l'utilisateur.
	 * @param idutilisateur ID de l'utilisateur.
	 * @param email         E-mail de l'utilisateur.
	 * @return Le JWT généré.
	 */
	public String generateToken(UserDetails userDetails, String role, Integer idutilisateur, String email) {
		Map<String, Object> extraClaims = new HashMap<>();
		extraClaims.put("role", role);
		extraClaims.put("idutilisateur", idutilisateur);
		extraClaims.put("email", email);

		Date issuedAt = new Date(System.currentTimeMillis());
		Date expiration = new Date(System.currentTimeMillis() + 1000L * 60 * 60 * 24 * 7);

		return Jwts.builder().setClaims(extraClaims).setSubject(userDetails.getUsername()).setIssuedAt(issuedAt)
				.setExpiration(expiration).signWith(getSignInKey(), SignatureAlgorithm.HS256).compact();
	}

	/**
	 * Extrait les informations de l'utilisateur à partir du JWT.
	 * 
	 * @param token Le JWT.
	 * @return Les informations de l'utilisateur.
	 */
	public Map<String, Object> extractUserData(String token) {
		Claims claims = extractAllClaims(token);
		String email = claims.getSubject();
		String role = claims.get("role", String.class);
		Integer idutilisateur = claims.get("idutilisateur", Integer.class);
		String fullname = claims.get("fullname", String.class);

		Map<String, Object> userData = new HashMap<>();
		userData.put("email", email);
		userData.put("role", role);
		userData.put("idutilisateur", idutilisateur);
		userData.put("fullname", fullname);

		return userData;
	}

	/**
	 * Vérifie si le JWT est valide.
	 * 
	 * @param token       Le JWT.
	 * @param userDetails Détails de l'utilisateur.
	 * @return true si le JWT est valide, sinon false.
	 */
	public boolean isTokenValid(String token, UserDetails userDetails) {
		final String email = extractUsername(token);
		return (email.equals(userDetails.getUsername())) && !isTokenExpired(token);
	}

	public boolean isTokenExpired(String token) {
		return extractExpiration(token).before(new Date());
	}

	private Date extractExpiration(String token) {
		return extractClaim(token, Claims::getExpiration);
	}

	private Claims extractAllClaims(String token) {
		if (token.startsWith("Bearer ")) {
			token = token.substring(7); // ignore "Bearer "
		}
		return Jwts.parserBuilder().setSigningKey(getSignInKey()).build().parseClaimsJws(token).getBody();
	}

	private Key getSignInKey() {
		byte[] keyBytes = Decoders.BASE64.decode(SECRET_KEY);
		return Keys.hmacShaKeyFor(keyBytes);
	}


}