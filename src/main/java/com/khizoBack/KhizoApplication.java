package com.khizoBack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KhizoApplication {

	public static void main(String[] args) {
		SpringApplication.run(KhizoApplication.class, args);
	}

}
