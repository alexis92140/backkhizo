package com.khizoBack.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.khizoBack.entity.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	 Optional<Role> findByNom(String nom);
}
