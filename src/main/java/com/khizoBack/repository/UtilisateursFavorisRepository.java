package com.khizoBack.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.khizoBack.entity.Utilisateur;
import com.khizoBack.entity.UtilisateursFavoris;

@Repository
public interface UtilisateursFavorisRepository extends JpaRepository<UtilisateursFavoris, Integer> { 
    List<UtilisateursFavoris> findByUtilisateur(Utilisateur utilisateur);
   
}
