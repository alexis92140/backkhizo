package com.khizoBack.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.khizoBack.entity.Evaluation;
import com.khizoBack.entity.Recette;
import com.khizoBack.entity.Utilisateur;

@Repository
public interface EvaluationRepository extends JpaRepository<Evaluation, Integer> {
  
    Evaluation findByUtilisateurAndRecette(Utilisateur utilisateur, Recette recette);
}
