package com.khizoBack.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.khizoBack.entity.Etape;

@Repository
public interface EtapeRepository extends JpaRepository<Etape, Integer> {
    List<Etape> findByRecetteIdrecette(Integer idRecette);

}


