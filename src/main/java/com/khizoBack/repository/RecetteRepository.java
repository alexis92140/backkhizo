package com.khizoBack.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.khizoBack.entity.Recette;
import com.khizoBack.entity.Utilisateur;

@Repository
public interface RecetteRepository extends JpaRepository<Recette, Integer> {
	
	  @Query("SELECT COUNT(r) FROM Recette r WHERE r.utilisateur.idutilisateur = :idutilisateur")
	    long countByUtilisateurIdutilisateur(@Param("idutilisateur") Integer idutilisateur);
	  long countByUtilisateur(Utilisateur utilisateur);

	List<Recette> findByUtilisateurIdutilisateur(Integer idutilisateur);

	@Query("SELECT r FROM Recette r JOIN r.utilisateur u WHERE u.email = :email")
	List<Recette> findRecettesByUtilisateurEmail(@Param("email") String email);
}
