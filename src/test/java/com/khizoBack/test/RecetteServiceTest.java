package com.khizoBack.test;

import com.khizoBack.entity.Recette;
import com.khizoBack.entity.Utilisateur;
import com.khizoBack.repository.RecetteRepository;
import com.khizoBack.service.RecetteService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
@TestPropertySource(locations = "classpath:application-test.properties")
public class RecetteServiceTest {

    @Test
    void contextLoads() {
    }


}
