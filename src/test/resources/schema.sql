CREATE TABLE   roles (
  idrole INT PRIMARY KEY AUTO_INCREMENT,
  nomrole VARCHAR(255)
);

CREATE TABLE utilisateurs (
  idutilisateur INT PRIMARY KEY AUTO_INCREMENT,
  email VARCHAR(255) UNIQUE,
  motdepasse VARCHAR(255),
  dateban TIMESTAMP,
  idrole INT,
  FOREIGN KEY (idrole) REFERENCES roles(idrole)
);

CREATE TABLE recettes (
  idrecette INT PRIMARY KEY AUTO_INCREMENT,
  idutilisateur INT,
  titre VARCHAR(255),
  estprivee BOOLEAN,
  dernieredatevisite TIMESTAMP,
  FOREIGN KEY (idutilisateur) REFERENCES utilisateurs(idutilisateur)
);

CREATE TABLE ingredients (
  idingredient INT PRIMARY KEY,
  nom VARCHAR(255)
);

CREATE TABLE ingredientsrecette (
  idrecette INT,
  idingredient INT,
  quantite VARCHAR(255),
  PRIMARY KEY (idrecette, idingredient),
  FOREIGN KEY (idrecette) REFERENCES recettes(idrecette),
  FOREIGN KEY (idingredient) REFERENCES ingredients(idingredient)
);

CREATE TABLE evaluations (
  idevaluation INT PRIMARY KEY,
  idutilisateur INT,
  idrecette INT,
  valeur INT,
  FOREIGN KEY (idutilisateur) REFERENCES utilisateurs(idutilisateur),
  FOREIGN KEY (idrecette) REFERENCES recettes(idrecette)
);

CREATE TABLE etapes (
  idetape INT PRIMARY KEY,
  idrecette INT,
  description VARCHAR(255),
  contenu TEXT,
  FOREIGN KEY (idrecette) REFERENCES recettes(idrecette)
);

CREATE TABLE utilisateursfavoris (
  id INT PRIMARY KEY,
  idrecette INT,
  idutilisateur INT,
  FOREIGN KEY (idrecette) REFERENCES recettes(idrecette),
  FOREIGN KEY (idutilisateur) REFERENCES utilisateurs(idutilisateur)
);

CREATE TABLE notifications (
  idnotification INT PRIMARY KEY,
  idutilisateur INT,
  contenu TEXT,
  datecreation TIMESTAMP,
  FOREIGN KEY (idutilisateur) REFERENCES utilisateurs(idutilisateur)
);

CREATE TABLE amities (
  idutilisateur1 INT,
  idutilisateur2 INT,
  PRIMARY KEY (idutilisateur1, idutilisateur2),
  FOREIGN KEY (idutilisateur1) REFERENCES utilisateurs(idutilisateur),
  FOREIGN KEY (idutilisateur2) REFERENCES utilisateurs(idutilisateur)
);

CREATE TABLE parametres (
  clef VARCHAR(255) PRIMARY KEY,
  valeur INT
);
