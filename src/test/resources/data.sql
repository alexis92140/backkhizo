-- Insérer dans la table 'roles'
INSERT INTO roles (idrole, nomrole) VALUES
(1, 'ROLE_USER'),
(2, 'ROLE_ADMIN');

-- Insérer dans la table 'utilisateurs'
INSERT INTO utilisateurs (idutilisateur, email, motdepasse, dateban, idrole) VALUES
(1, 'alex@alex.alex', 'alex', NULL, 1),
(2, 'bob@bob.bob', 'bob', NULL, 2);


-- Insérer dans la table 'recettes'
INSERT INTO recettes (idrecette, idutilisateur, titre, estprivee, dernieredatevisite) VALUES
(1, 1, 'Recette de Gâteau', FALSE, '2023-01-01 10:00:00'),
(2, 2, 'Recette de Pizza', TRUE, '2023-01-02 10:00:00');

-- Insérer dans la table 'ingredients'
INSERT INTO ingredients (idingredient, nom) VALUES
(1, 'Farine'),
(2, 'Tomate');

-- Insérer dans la table 'ingredientsrecette'
INSERT INTO ingredientsrecette (idrecette, idingredient, quantite) VALUES
(1, 1, '200g'),
(2, 2, '3');

-- Insérer dans la table 'evaluations'
INSERT INTO evaluations (idevaluation, idutilisateur, idrecette, valeur) VALUES
(1, 1, 1, 5),
(2, 2, 2, 4);

-- Insérer dans la table 'utilisateursfavoris'
INSERT INTO utilisateursfavoris (id, idrecette, idutilisateur) VALUES
(1, 1, 1),
(2, 2, 2);

-- Insérer dans la table 'etapes'
INSERT INTO etapes (idetape, idrecette, description, contenu) VALUES
(1, 1, 'Etape 1', 'Mélanger la farine et les œufs'),
(2, 2, 'Etape 2', 'Ajouter la garniture sur la pâte');

-- Insérer dans la table 'notifications'
INSERT INTO notifications (idnotification, idutilisateur, contenu, datecreation) VALUES
(1, 1, 'Votre recette a été évaluée à 5 étoiles!', '2023-01-01 12:00:00');

-- Insérer dans la table 'amities'
INSERT INTO amities (idutilisateur1, idutilisateur2) VALUES
(1, 2),
(2, 1);

-- Insérer dans la table 'parametres'
INSERT INTO parametres (clef, valeur) VALUES
('theme', '2'),
('langue', '2');
