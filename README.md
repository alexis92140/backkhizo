# Khizo 2 O Renascente
# Por Alexis Breen

O Khizo é um aplicativo de receitas culinárias; torne-se o chef que sempre sonhou em ser. Embora na verdade você seja um desastre, até mesmo as massas instantâneas são um desafio para você, então boa sorte.

# Tecnologias Utilizadas

- Java
- Spring Framework
- Spring Security

# Instalação e Execução

1. Clone o repositório do GitHub.

2. Compile e execute a aplicação utilizando o Maven: `mvn spring-boot:run`.

3. Acesse a aplicação no seu navegador pelo endereço http://localhost:8080.

# Contribuições

Contribuições para o projeto Khizo são bem-vindas! Se deseja contribuir, siga os passos abaixo:

1. Faça um fork do projeto.

2. Crie um branch para sua funcionalidade: `git checkout -b nome-do-branch`.

3. Faça suas modificações e adicione-as: `git add .`.

4. Confirme suas modificações: `git commit -m "Descrição das modificações"`.

5. Envie suas modificações para o seu repositório: `git push origin nome-do-branch`.

6. Submeta um pull request no GitHub.
